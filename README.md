# Detalhes do projeto

##	Algumas considerações: Recebi as instruções para o projeto na sexta 13, porém tive pós graduação na sesta e sabado e meu tempo foi muito curto, em virtude disso acabei penalizando alguns aspectos do projeto por ex: 
* Tratamento de erros simples embora consiga fazer uma boa separação dos tipos de erros.
* Não tive tempo suficiente para refinar os testes automáticos embora estejam presentes
* Não consegui fazer a animação da de transição da lista para para o detalhe do vídeo, então decidi utilizar outra abordagem.
* Tive alguns problemas com as limitações de requisições da Youtube Data API, o que resultou em uma mínima duplicação de código no mapeamento dos objetos. Cabe uma refatoração.
* Desconheço o uso de injeção de dependência no swift, não tive tempo para estudar no momento, mas irei.

Utilizei o design arquitetural MVP. Criei um branch para poder fazer algumas implementações maiores, mas já re-integrei ao branch master.
	Todos os testes foram realizados utilizando o simulador dos devices IPhone 11 max com a versão do IOS 13.1 e IPhone 6 s com a versão do IOS 12.4.
	Como não teria muito tempo disponível decidi travar a orientação em portrait.
	Os testes foram realizados sobre a camada de serviço, com o intuito de garantir que algum resultado está sendo retornado e que as mensagens de erros não estão sendo suprimidas, e no presenter para garantir que as classes que implementarem os seus respectivos protocolos irão receber as notificações dos eventos.
	Utilizei a API de cache e download de imagens kingsfisher, para tal tive que adicionar o cocoapods, logo o projeto deve ser aberto a partir do arquivo MadeInWebYoutubeSearchApp.xcworkspace
	Antes da primeiro build o comando pod install deverá ser executado, com o intuito instalar as dependências.
