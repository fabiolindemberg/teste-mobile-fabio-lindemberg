//
//  SearchPresenter.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 14/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import Foundation

protocol SearchPresenterView: class {
    func beforeFetch()
    func afterFetch()
    func beforeFetchNextPage()
    func afterFetchNextPage()
    func show(error message: String)
}

class SearchPresenter {
    private var isFetchingNextPage = false
    private var pageCount = 1
    private var pageSize = 15
    private var pageToken: String?
    private var items: [YoutubeItem] = []
    private weak var view: SearchPresenterView?
    
    init(with view: SearchPresenterView) {
        self.view = view
    }
    
    func numberOfItems() -> Int {
        return items.count
    }
    
    func maxResult() -> Int {
        return pageCount * pageSize
    }
    
    func item(by index: Int) -> YoutubeItem {
        return items[index]
    }
    
    func clearResults() {
        items = []
        view?.afterFetch()
    }
    
    func fetchNextPage() {
        
        guard pageToken != nil else {
            return
        }
        
        guard !isFetchingNextPage else {
            return
        }
        
        fetch(with: SearchParameter(q: nil, pageToken: pageToken))
        pageCount += 1
    }
    
    func fetch(by text: String) {
        let q = removeBlankSpaces(from: text)
        fetch(with: SearchParameter(q: q, pageToken: nil))
    }
    
    private func runBeforeFetchEvent(_ searchParameter: SearchParameter) {
        if searchParameter.pageToken != nil {
            isFetchingNextPage = true
            view?.beforeFetchNextPage()
        } else {
            view?.beforeFetch()
        }
    }
    
    private func runAfterFetchEvent(_ searchParameter: SearchParameter) {
        if searchParameter.pageToken != nil {
            isFetchingNextPage = false
            view?.afterFetchNextPage()
        } else {
            view?.afterFetch()
        }
    }
    
    private func fetch(with searchParameter: SearchParameter) {
        
        runBeforeFetchEvent(searchParameter)
        
        YoutubeService.shared.fetchList(with: searchParameter, onSuccess: { (items,nextPageToken) in
            self.runAfterFetchEvent(searchParameter)
            self.items.append(contentsOf: items)
            self.pageToken = nextPageToken
        }) { (error) in
            self.view?.show(error: error.rawValue)
        }
    }
    
    private func removeBlankSpaces(from text: String) -> String {
        let words = text.split(separator: " ")
        return words.joined(separator: "+")
    }
    
}
