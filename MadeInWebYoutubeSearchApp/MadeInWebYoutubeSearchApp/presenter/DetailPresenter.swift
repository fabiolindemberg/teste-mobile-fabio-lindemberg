//
//  DetailPresenter.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 17/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import Foundation

protocol DatailPresenterView: class {
    func onDetail(item: YoutubeDetail)
    func show(error: String)
}

class DetailPresenter {
    
    private weak var view: DatailPresenterView?
    
    init(with view: DatailPresenterView) {
        self.view = view
    }
    
    func detail(by id: String) {
        YoutubeService.shared.fetchDetail(with: id, onSuccess: { (items) in
            if let item = items.first {
                self.view?.onDetail(item: item)
            }
        }) { (error) in
            self.view?.show(error: error.rawValue)
        }
    }
}
