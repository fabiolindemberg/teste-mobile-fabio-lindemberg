//
//  YoutubeService.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 14/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import Foundation

class YoutubeService {
    
    private let baseUrl = "https://www.googleapis.com/youtube/v3/"
    private let apiToken = "AIzaSyD1J5xIVDlZgMCaXrV-Suxg0VKMlTfMhUc"
    private let maxResults = 15
    static let shared = YoutubeService()
    
    func fetchDetail(with id: String, onSuccess: @escaping (_ itens: [YoutubeDetail])->Void, onError: @escaping (_ error: CustomError)->Void) {
        let stringUrl =  "\(baseUrl)videos?id=\(id)&part=snippet,statistics&key=\(apiToken)"
        
        guard let url = URL(string: stringUrl) else {
            onError(.invalidUrl)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                onError(.error)
                return
            }
            
            guard data != nil else {
                onError(.noData)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    do {
                        let dataString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                        print(dataString)
                        let detailContainer = try JSONDecoder().decode(DetailContainer.self, from: data!)
                        //print("Total results: \(itens.pageInfo.totalResults)")
                        onSuccess(detailContainer.items)
                    } catch {
                        print(error)
                        onError(.invalidJson)
                    }
                } else {
                    let dataString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    print(dataString)
                    
                    onError(.error)
                }
            }
        }.resume()

    }
    
    func fetchList(with searchParameter: SearchParameter, onSuccess: @escaping (_ itens: [YoutubeItem],_ pageToken: String?)->Void, onError: @escaping (_ error: CustomError)->Void) {
        
        let stringUrl = "\(baseUrl)search?part=id,snippet\(searchParameter.toString())&type=video&maxResults=\(maxResults)&key=\(apiToken)"
        guard let url = URL(string: stringUrl) else {
            onError(.invalidUrl)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                onError(.error)
                return
            }
            
            guard data != nil else {
                onError(.noData)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    do {
                        let paginationContainer = try JSONDecoder().decode(PaginationContainer.self, from: data!)
                        //print("Total results: \(itens.pageInfo.totalResults)")
                        onSuccess(paginationContainer.items, paginationContainer.nextPageToken)
                    } catch {
                        print(error.localizedDescription)
                        onError(.invalidJson)
                    }
                } else {
                    let dataString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    print(dataString)
                    
                    onError(.error)
                }
            }
        }.resume()
            
    }
}

struct Erro: Codable {
    let domain: String
    let reason: String
    let message: String
    let extendedHelp: String
}

struct SearchParameter {
    let q: String?
    let pageToken: String?
    
    func toString() -> String {
        return q != nil ? "&q=\(q!)" : "&pageToken\(pageToken!)"
    }
}
