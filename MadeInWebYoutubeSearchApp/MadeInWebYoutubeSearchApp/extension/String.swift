//
//  String.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 17/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import Foundation

extension String {
    var forSorting: String {
        let simple = folding(options: [.diacriticInsensitive, .widthInsensitive, .caseInsensitive], locale: nil)
        let nonAlphaNumeric = CharacterSet.alphanumerics.inverted
        return simple.components(separatedBy: nonAlphaNumeric).joined(separator: " ")
    }
}
