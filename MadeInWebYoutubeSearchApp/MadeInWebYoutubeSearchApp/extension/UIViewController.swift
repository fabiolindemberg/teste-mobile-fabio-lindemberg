//
//  UIViewController.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 17/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import UIKit

extension UIViewController {
    public func dismissKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc public func dismissKeyboard() {
        view.endEditing(true)
    }

}
