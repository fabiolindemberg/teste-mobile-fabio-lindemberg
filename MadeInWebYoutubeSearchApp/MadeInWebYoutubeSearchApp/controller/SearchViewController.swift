//
//  SearchViewController.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 14/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    // Layout elements
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var madeInWebLogoHeight: NSLayoutConstraint!
    @IBOutlet weak var rootStackView: UIStackView!
    @IBOutlet weak var searchStackView: UIStackView!
    @IBOutlet weak var searchTextHeight: NSLayoutConstraint!
    
    // Components
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var medeInWebLogo: UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchText: UITextField!
    
    // Constants
    private let animationDuration = 0.1
    private let rootStackViewSpacingSearchModeValue = CGFloat(50.0)
    private let searchStackViewSpacingSearchModeValue = CGFloat(8.0)
    private let topConstraintSearchModeValue = CGFloat(200.0)
    private let madeInWebLogoHeightSearchModeValue = CGFloat(50.0)
    private let fontSizeSearchModeValue = CGFloat(20)
    private let searchTextHeigthSearchModeValue = CGFloat(50.0)
    private let rootStackViewSpacingShowResultsModeValue = CGFloat(24.0)
    private let searchStackViewSpacingShowResultsModeValue = CGFloat(0.0)
    private let madeInWebLogoHeightShowResultsModeValue = CGFloat(32.0)
    private let topConstraintShowResultsModeValue = CGFloat(8.0)
    private let fontSizeShowResultsModeValue = CGFloat(15)
    private let searchTextHeigthShowResultsModeValue = CGFloat(40.0)
    private let itemCellIdentifier = "itemCell"
    private let searchPlaceHolder = "Pesquisar"
    
    lazy var searchPresenter = SearchPresenter(with: self)
    
    private var isLoadingNextPage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        setupMadeInWebLogo()
    }
    
    private func setupTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        dismissKeyboardWhenTappedAround()
    }
    
    private func setupMadeInWebLogo() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.onMadeInWebLogoTapped(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        
        medeInWebLogo.isUserInteractionEnabled = true
        medeInWebLogo.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func onMadeInWebLogoTapped(_ sender: UITapGestureRecognizer){
        UIView.animate(withDuration: animationDuration) {
            self.setupSearchModeAppearence()
        }
        clearData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupSearchModeAppearence()
    }

    private func setupSearchModeAppearence() {
        topConstraint.constant = self.topConstraintSearchModeValue

        madeInWebLogoHeight.constant = madeInWebLogoHeightSearchModeValue

        rootStackView.spacing = rootStackViewSpacingSearchModeValue
        
        searchStackView.axis = .vertical
        searchStackView.spacing = searchStackViewSpacingSearchModeValue
        
        searchButton.titleLabel?.font = searchButton.titleLabel?.font.withSize(fontSizeSearchModeValue)
        
        searchTextHeight.constant = searchTextHeigthSearchModeValue
        
        searchText.font?.withSize(fontSizeSearchModeValue)
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSizeSearchModeValue, weight: .regular)]

        searchText.attributedPlaceholder = NSAttributedString(string: searchPlaceHolder, attributes:attributes)
    }
    
    private func clearData() {
        searchPresenter.clearResults()
        searchText.text = ""
    }
    
    @IBAction func onSearchButtonClick(sender: UIButton) {
        
        searchPresenter.clearResults()
        
        guard searchText.text ?? "" != "" else {
            searchText.becomeFirstResponder()
            searchText.shake()
            return
        }
        
        UIView.animate(withDuration: animationDuration) {
            self.setupShowResultsModeAppearence()
        }
        
        fetchData(searchText: searchText.text!.forSorting)
    }
    
    private func setupShowResultsModeAppearence() {
        topConstraint.constant = self.topConstraintShowResultsModeValue
        
        rootStackView.spacing = rootStackViewSpacingShowResultsModeValue

        madeInWebLogoHeight.constant = madeInWebLogoHeightShowResultsModeValue

        searchStackView.axis = .horizontal
        
        searchStackView.spacing = searchStackViewSpacingShowResultsModeValue
        
        searchButton.titleLabel?.font = searchButton.titleLabel?.font.withSize(fontSizeShowResultsModeValue)

        searchTextHeight.constant = searchTextHeigthShowResultsModeValue
        
        searchText.font?.withSize(fontSizeShowResultsModeValue)
        let attributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: fontSizeShowResultsModeValue, weight: .regular)]

        searchText.attributedPlaceholder = NSAttributedString(string: searchPlaceHolder, attributes:attributes)
    }

    private func fetchData(searchText: String) {
        searchPresenter.fetch(by: searchText)
    }
    
    private func showAlert(text: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alerta", message: text, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let itemViewController = segue.destination as! ItemViewController
        itemViewController.id = searchPresenter.item(by: tableView.indexPathForSelectedRow!.row).id?.videoId
    }

}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView.indexPathsForVisibleRows?.last == indexPath && indexPath.row == searchPresenter.maxResult()-1 {
            searchPresenter.fetchNextPage()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchPresenter.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: itemCellIdentifier, for: indexPath) as! ItemTableViewCell
        cell.fillItemInfo(item: searchPresenter.item(by: indexPath.row))
        
        return cell
    }
}

extension SearchViewController: SearchPresenterView {
    func beforeFetch() {
        DispatchQueue.main.async {
            let activityIndicator = UIActivityIndicatorView()
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            
            self.tableView.backgroundView = activityIndicator
        }
    }
    
    func beforeFetchNextPage() {
        DispatchQueue.main.async {
            let activityIndicator = UIActivityIndicatorView()
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            self.tableView.tableFooterView = activityIndicator
        }
    }
    
    func afterFetch() {
        DispatchQueue.main.async {
            (self.tableView.backgroundView as? UIActivityIndicatorView)?.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    func afterFetchNextPage() {
        DispatchQueue.main.async {
            (self.tableView.tableFooterView as? UIActivityIndicatorView)?.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    func show(error message: String) {
        DispatchQueue.main.async {
            self.showAlert(text: message)
        }
    }
    
}
