//
//  CustomError.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 13/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import Foundation

enum CustomError: String {
    case invalidUrl = "Url inválida"
    case invalidJson = "Json inválido"
    case noData = "Sem dados"
    case error = "Retorno inesperado"
}
