//
//  ItemViewController.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 14/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import UIKit

class ItemViewController: UIViewController {

    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemVisualization: UILabel!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    
    lazy var detailPresenter = DetailPresenter(with: self)
    var id: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard id != nil else {
            return
        }
        
        detailPresenter.detail(by: self.id!)
    }
    
    func fillItemInfo(item: YoutubeDetail) {
        itemTitle.text = item.snippet.title
        itemDescription.text = item.snippet.description
        
        if let visualizations = item.statistics?.viewCount {
            itemVisualization.text = "Visualizações \(visualizations)"
        }
            if let stringUrl = item.snippet.thumbnails?.medium?.url {
            if let url = URL(string: stringUrl) {
                itemImage.kf.setImage(with: url)
            }
        }

    }
    
}

extension ItemViewController: DatailPresenterView {
    func onDetail(item: YoutubeDetail) {
        DispatchQueue.main.async {
            self.fillItemInfo(item: item)
        }
    }
    
    func show(error: String) {
        //
    }
    
    
}
