//
//  ItemTableViewCell.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 14/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import UIKit
import Kingfisher

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itenImage: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func fillItemInfo(item: YoutubeItem) {
        itemTitle.text = item.snippet.title
        itemDescription.text = item.snippet.description
        
        if let stringUrl = item.snippet.thumbnails?.medium?.url {
            if let url = URL(string: stringUrl) {
                itenImage.kf.setImage(with: url)
            }
        }
    }
}
