//
//  YoutubeItem.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 13/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import Foundation

struct YoutubeItem: Codable {
    let id: Id?
    let statistics: Statistic?
    let snippet: Snippet
}

struct YoutubeDetail: Codable {
    let id: String?
    let statistics: Statistic?
    let snippet: Snippet
}

struct Snippet: Codable {
    let title: String?
    let description: String?
    let thumbnails: Thumbnails?
}

struct Thumbnails: Codable {
    let medium: ImageInfo?
    let high: ImageInfo?
}

struct ImageInfo: Codable {
    let url: String?
    let width: Int?
    let height: Int?
}

struct Id: Codable {
    let kind: String?
    let videoId: String?
}

struct Statistic: Codable {
    let viewCount: String?
    let likeCount: String?
}


