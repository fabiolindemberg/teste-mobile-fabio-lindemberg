//
//  PaginationContainer.swift
//  MadeInWebYoutubeSearchApp
//
//  Created by Fabio Estudo on 14/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import Foundation

struct PaginationContainer: Codable {
    let kind: String
    let nextPageToken: String?
    let pageInfo: PageInfo
    let items: [YoutubeItem]
}

struct DetailContainer: Codable {
    let kind: String
    let pageInfo: PageInfo
    let items: [YoutubeDetail]
}


struct PageInfo: Codable {
    let totalResults: Int
    let resultsPerPage: Int
}
