//
//  MadeInWebYoutubeSearchAppTests.swift
//  MadeInWebYoutubeSearchAppTests
//
//  Created by Fabio Estudo on 16/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import XCTest

class MadeInWebYoutubeSearchAppTests: XCTestCase {

    var expectedNumberOfItens: Int?
    var beforeFetchHasRun = false
    var beforeFetchNextPageHasRun = false
    var afterFetchHasRun = false
    var afterFetchNextPageHasRun = false
    var showErrorHasRun = false
    var searchPresenter: SearchPresenter?
    var expect: XCTestExpectation?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        searchPresenter = SearchPresenter(with: self)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetch() {
        expect = expectation(description: "wait for fetch")
        searchPresenter?.fetch(by: "eva andressa")
        wait(for: [expect!], timeout: 10)
        
        XCTAssert(beforeFetchHasRun)
        XCTAssert(afterFetchHasRun)
    }
    
    func testFetchNextPage() {
        expect = expectation(description: "wait for fetch")
        searchPresenter?.fetchNextPage()
        wait(for: [expect!], timeout: 10)
        
        XCTAssert(beforeFetchNextPageHasRun)
        XCTAssert(afterFetchNextPageHasRun)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}

extension MadeInWebYoutubeSearchAppTests: SearchPresenterView {
    func show(error message: String) {
        showErrorHasRun = true
        expect?.fulfill()
    }
    
    func beforeFetchNextPage() {
        beforeFetchNextPageHasRun = true
        expect?.fulfill()
}
    
    func beforeFetch() {
        beforeFetchHasRun = true
        expect?.fulfill()
    }
    
    func afterFetchNextPage() {
        afterFetchNextPageHasRun = true
        expect?.fulfill()
}
    
    func afterFetch() {
        afterFetchHasRun = true
        expect?.fulfill()
    }
}


