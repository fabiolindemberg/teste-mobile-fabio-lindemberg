//
//  YoutubeServiceTest.swift
//  MadeInWebYoutubeSearchAppTests
//
//  Created by Fabio Estudo on 17/12/19.
//  Copyright © 2019 Fabio Estudo. All rights reserved.
//

import XCTest

class YoutubeServiceTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEFetchDetail() {
        
        let expect = expectation(description: "Wait for API response")
        
        let itemId = "g2wlyIkzGOU"
        YoutubeService.shared.fetchDetail(with: itemId, onSuccess: { (items) in
            XCTAssertNotNil(items)
            expect.fulfill()
        }, onError: { (error) in
            XCTAssertNil(error)
            expect.fulfill()
        })
        
        wait(for: [expect], timeout: 10)
    }

    func testEFetchList() {
        
        let expect = expectation(description: "Wait for API response")
        
        YoutubeService.shared.fetchList(with: SearchParameter(q: "eva andressa", pageToken: nil), onSuccess: { (items, page) in
            XCTAssertNotNil(items)
            expect.fulfill()
        }, onError: { (error) in
            XCTAssertNil(error)
            expect.fulfill()
        })
        
        wait(for: [expect], timeout: 10)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
